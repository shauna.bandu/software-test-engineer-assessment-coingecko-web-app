import { errorMsg, landingPageTxt } from "../../fixtures/text";
import { buttons, modals, inputFields, error, alerts } from "../../fixtures/paths";
import { email, pw } from "../../fixtures/login";
import { error } from "cypress/types/jquery";

class LoginElements {
    elements = {
        signUpBtn: () => cy.get(buttons.loginBtns).contains(landingPageTxt.signUpBtnTxt),
        submitSignUpBtn: () => cy.get(buttons.submitSignUpBtn),
        
        loginTxt: () => cy.get(landingPageTxt.loginBtn),

        authModal: () => cy.get(modals.authModal),

        emailField: () => cy.get(inputFields.email),
        pwField: () => cy.get(inputFields.password),

        errorMsg: () => cy.get(error.pwMsg),
        activateAcctMsg: () => cy.get(alerts.landingPageMsg),
    }

    clickSignUpButton() {
        this.elements.signUpBtn().click();
        this.elements.authModal().should("be.visible");
    }
    
    enterUserCredentials(email, password) {
        this.elements.emailField().clear().type(email);
        this.elements.pwField().type(password);
        this.elements.submitSignUpBtn().click();
    }

    verifyValidSignUp() {
        this.enterUserCredentials(email.valid, pw.valid);
        this.elements.submitSignUpBtn().click();
        this.elements.activateAcctMsg().should("contain", landingPageTxt.activateAcct);
    }

    verifyWeakPassword() {
        this.enterUserCredentials(email.valid, pw.weak);
        this.elements.submitSignUpBtn().click();
        this.elements.errorMsg().should("contain", errorMsg.weakPw);
    }
}

export default LoginElements;