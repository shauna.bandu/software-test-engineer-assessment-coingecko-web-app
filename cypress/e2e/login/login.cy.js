import LoginElements, { clickLogin, enterUserCredentials } from "./login_fn";

const loginElem = new LoginElements();

describe('Login tests', () => {

    beforeEach(() => {
        cy.visit("https://www.coingecko.com");
        loginElem.clickSignUpButton();


    })
    it('Signup with correct user credentials', () => {
        loginElem.verifyValidSignUp();
    });

    it('Verify weak password check', () => {
        loginElem.verifyWeakPassword();

    });
});

