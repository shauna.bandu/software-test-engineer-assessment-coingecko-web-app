import { buttons, inputFields, modals, searchResults, tables } from "../../fixtures/paths"
import { urls } from "../../fixtures/urls";
import {faker} from "@faker-js/faker";
import { portfolioTxt } from "../../fixtures/text";

var transaction = {};

export function genTransactionObj() {
    transaction.ppc = faker.number.int({min: 1, max: 100000}),
    transaction.quantity = faker.number.float({min: 0.00001, max: 10000, multipleOf: 0.00001}),
    transaction.fee = faker.number.int({min: 1, max: 10000}),
    transaction.notes = faker.word.words(4);

    cy.writeFile("cypress/fixtures/readWrite/transaction.json", "");
    cy.writeFile("cypress/fixtures/readWrite/transaction.json", {transaction});

    return transaction;
}

class PortfolioElements {

    elements = {

        //Buttons
        allPortfoliosBtn: () => cy.get(buttons.allPortfoliosBtn),
        portfolioButton: () => cy.get(buttons.pfolio),
        addCoinButton: () => cy.get(buttons.addCoinsBtn),
        addTransactionButton: () => cy.get(buttons.addTransactionBtn),
        addCoinsLink: () => cy.get(buttons.addCoinsLink),
        submitTransactionButton: () => cy.get(`${modals.addTransactionModal} ${buttons.submitTransactionBtn}`),
        feesNotesButton: () => cy.get(`${modals.addTransactionModal} ${buttons.feesNotesBtn}`),

        //Table
        cgTable: () => cy.get(tables.cgTable),
        cgTableFirstRow: () => cy.get(tables.cgTableFirstRow),
        coinColumn: () => cy.get(`${tables.cgTableFirstRow} ${tables.coinNameColumn} span:nth-child(1)`),
        priceColumn: () => cy.get(`${tables.cgTableFirstRow} ${tables.priceColumn}`),
        feesColumn: () => cy.get(`${tables.cgTableFirstRow} ${tables.feesColumn}`),
        notesColumn: () => cy.get(`${tables.cgTableFirstRow} ${tables.notesColumn}`),
        quantityColumn: () => cy.get(`${tables.cgTableFirstRow} ${tables.quantityColumn}`),

        //Modal
        addCoinModal: () => cy.get(modals.addCoinModal),
        addTransactionModal: () => cy.get(modals.addTransactionModal),

        //Input Fields
        searchCoinField: () => cy.get(inputFields.searchCoinField),
        quantityField: () => cy.get(inputFields.quantityField),
        ppcField: () => cy.get(inputFields.ppcField),
        feesField: () => cy.get(inputFields.transactionFeesField),
        notesField: () => cy.get(inputFields.notesField),

        //Search Results
        coinResults: () => cy.get(searchResults.coinsSearchResults),

        //Text
        addCoinTxt: () => cy.get(portfolioTxt.addCoinTxt),
    }

    clickPortfolioBtn() {
        this.elements.portfolioButton().click();
        cy.get('a').should("have.attr", "href").and('contains', urls.allPortfolio);
        this.elements.allPortfoliosBtn().click();
        cy.url().should("contain", urls.allPortfolio);
    }

    verifyEmptyPortfolio() {
        this.clickPortfolioBtn();
        this.elements.cgTable().should("be.visible");
        this.elements.addCoinTxt().should("be.visible")
        this.elements.addCoinsLink().should("be.visible");
    }

    addCoinToPortfolio(coin) {
        this.clickPortfolioBtn();
        this.elements.addCoinButton().click();
        this.elements.addCoinModal().should("be.visible");
        this.elements.searchCoinField().click().type(coin);
        this.elements.coinResults().first().should("contain", coin)
        this.elements.coinResults.contains(coin).click();
        this.elements.coinColumn().should("contain", coin);
    }

    addTransactionToPortfolio() {
        this.clickPortfolioBtn();
        this.elements.addTransactionButton().click();
        this.elements.addTransactionModal().should("be.visible");

        cy.readFile("cypress/fixtures/readWrite/transaction.json").then((value) => {
            let trans = value.transaction;
            this.elements.ppcField().type(trans.ppc);
            this.elements.quantityField().type(trans.quantity);
            this.elements.feesNotesButton().click();
            this.elements.feesField().type(trans.fee);
            this.elements.notesField().type(trans.notes);
            this.elements.submitTransactionButton();
            this.elements.addTransactionModal().should("not.be.visible");
        });
    }

    verifyTransaction() {
        cy.readFile("cypress/fixtures/readWrite/transaction.json").then((value) => {
            let trans = value.transaction;
            this.elements.priceColumn().should("contain", trans.ppc)
            this.elements.quantityColumn().should("contain", trans.quantity)
            this.elements.feesColumn().should("contain", trans.fees)
            this.elements.notesColumn().should("contain", trans.notes)
        });    
    }
}

export default PortfolioElements;