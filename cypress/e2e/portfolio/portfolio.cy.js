import PortfolioElements, { clickPortfolioBtn, genTransactionObj } from "./portfolio_fn";
import { buttons, inputFields, modals, searchResults, tables } from "../../fixtures/paths";
import { portfolio } from "../../fixtures/text";
import { clickLogin } from "../login/login_fn";

const inputVal = require("../../fixtures/login.json");

const portfolioElem = new PortfolioElements();

describe("Portfolio tests", () => {

    beforeEach(() => {
        cy.visit("https://www.coingecko.com");
        clickLogin();
        enterUserCredentials(inputVal.email.valid, inputVal.pw.valid);
        //Submit form button
    })
    it("Access Portfolio with no coins", () => {
        portfolioElem.verifyEmptyPortfolio();
    });

    it("Add coin to portfolio", () => {
        portfolioElem.addCoinToPortfolio("BTC");
    });

    it("Add transaction to portfolio", () => {
        genTransactionObj();
        portfolioElem.addTransactionToPortfolio();
        portfolioElem.verifyTransaction();
    })
})