export const email = {
    valid: "testemail@test.com",
    invalid: "testtest"
}

export const pw = {
    short: "abcd",
    weak: "abcdefghijklmnop",
    valid: "Abcd1234!"
}