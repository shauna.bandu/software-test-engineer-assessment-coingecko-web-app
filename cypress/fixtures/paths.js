export const buttons = {
    loginBtns: 'button[data-login-state-target="loggedOutItem"]',
    submitSignUpBtn: 'button[data-auth-target="signUpSubmit"]',
    pfolio: 'div[data-login-state-target="portfolioItem"]',
    addCoinsLink: 'a[data-target="#addCoinModal"]',
    addCoinsBtn: 'button[data-portfolios-target="addCoinButton"]',
    addTransactionBtn: '.coingecko-table tbody tr:nth-child(1) i.fa-plus',
    submitTransactionBtn: 'input[type="submit"]',
    feesNotesBtn: 'span[data-target="#portfolio-coin-transaction-optional-field"] > span',
    allPortfoliosBtn: 'a[href="en/portfolios/portfolio_overview"]',
}

export const modals = {
    authModal: 'div[id="auth-modal"]',
    addCoinModal: '#addNewCoinModal',
    addTransactionModal: '#new_transaction_modal .portfolio-coin-transaction-form'
}

export const inputFields = {
    email: '#sign-up #user_email',
    pw: '#sign-up #user_password',
    searchCoinField: 'input[placeholder="Enter Coin Name"]',
    quantityField: '#portfolio-coin-transactions-new_portfolio_coin_transaction_quantity',
    ppcField: '#preview_spent_input',
    totalSpentField: '#preview_spent_output',
    transactionFeesField: '#portfolio-coin-transactions-new_portfolio_coin_transaction_fees',
    notesField: '#portfolio-coin-transactions-new_portfolio_coin_transaction_notes',
}

export const error = {
    pwMsg: 'div[data--target="ErrorMessage"]',
}

export const tables = {
    cgTable: ".coingecko-table",
    cgTableFirstRow: 'tbody tr:nth-child(1)',
    coinNameColumn: 'td.coin-name',
    priceColumn: 'td.price',
    quantityColumn: 'td:nth-child(3)',
    costColumn: 'td.cost',
    notesColumn: 'td.tw-max-w-sm',
    feesColumn: 'td.fees'
}

export const searchResults = {
    coinsSearchResults: 'ul.list-group li',
}

export const alerts = {
    landingPageMsg: '#unobtrusive-flash-messages',
}