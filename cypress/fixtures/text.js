export const landingPageTxt = {
    signUpBtnTxt: "Sign Up",
    activateAcct: 'A message with a confirmation link has been sent to your email address. Please follow the link to activate your account.',
}

export const errorMsg = {
    shortPw: "Password is too short (minimum is 8 characters).",
    weakPw: "Password is not strong, please consider adding numbers, symbols or more letters to keep account secure."
}

export const portfolioTxt = {
    noCoins: "You do not have any favorite coins yet.",
    addCoinTxt: "Add a new coin to get started!",
}